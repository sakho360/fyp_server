const mongoose = require("mongoose");
mongoose.set("debug", true);
mongoose.Promise = Promise;
mongoose.connect("mongodb://oscar:apples1510@ds247001.mlab.com:47001/traffic_watch", {
  keepAlive: true
});

module.exports.User = require("./user");
module.exports.Trip = require("./trip");
