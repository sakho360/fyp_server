const db = require("../models");

exports.createTrip = async function(req, res, next) {
  try {
    let trip = await db.Trip.create({
			user: [
				 req.params.id,
		],
			location_to: req.body.location_to,
			location_from: req.body.location_from,
			location_from_id: req.body.location_from_id,
			location_to_id: req.body.location_to_id,
			timeStamp: req.body.timeStamp,
			spaces: req.body.spaces,
			active: false,
			completed: false,
			user_confirm: {
				id: ' ',
				completed: false,
			},
			owner: {
					 name: req.body.owner,
					 id: req.params.id,
					 completed: false,
			},
			spaces: req.body.spaces,
			dateISO: req.body.dateISO,
			distanceTravelled: '',

    });
    let foundUser = await db.User.findById(req.params.id);
    foundUser.trips.push(trip.id);
    await foundUser.save();
    let foundTrip = await db.Trip.findById(trip._id).populate("user", {
      name: true,
		});
		
    return res.status(200).json(foundTrip);
  } catch (err) {
    return next(err);
  }
};


// GET - /api/users/:id/trips/:trip_id
exports.getTrip = async function(req, res, next) {
  try {
		let trip = await db.Trip.findById(req.params.trip_id);
    return res.status(200).json(trip);
  } catch (err) {
    return next(err);
  }
};

// DELETE /api/users/:id/trips/:trip_id
exports.deleteTrip = async function(req, res, next) {
  try {
    let foundTrip = await db.Trip.findById(req.params.trip_id);
    await foundTrip.remove();

    return res.status(200).json(foundTrip);
  } catch (err) {
    return next(err);
  }
};

// Specfic routes, may be a better way to do this

// UPDATE /api/users/:id/trips/:trip_id
// On user confirm of a trip
exports.updateTrip = async function(req, res, next) {
	try {
		let foundTrip = await db.Trip.findById(req.params.trip_id);
		let trip = await foundTrip.update({
			 active: true,
			 "user_confirm.id" : req.params.id,
			 
		});
		return res.status(200).json(trip);

		
	} catch(err) {
		return next(err);
	}
}

// UPDATE /api/users/:id/trips/:trip_id/trip-complete-user
// On trip completion user
exports.prelimTripComplete = async function(req,res,next) {
	try {
		let foundTrip = await db.Trip.findById(req.params.trip_id);
		let foundUser = await db.User.findById(req.params.id)

		let trip = await foundTrip.update({
			
			"owner.completed": req.body.ownerTrue ,
			"user_confirm.completed": req.body.userTrue,
			distanceTravelled: req.body.distanceTravelled,
			tripScore: req.body.tripScore 
	
			});


		return res.status(200).json(trip);

	} catch (error) {
		return next(error);
	}
}

// UPDATE /api/users/:id/:trip_id/tripComplete
// On trip completion  - restricted route only if both driver and passenger complete triip
exports.tripComplete = async function(req,res,next) {
	try {
		let foundTrip = await db.Trip.findById(req.params.trip_id);
		let foundUser = await db.User.findById(req.params.id)

		let trip = await foundTrip.update({
			completed: true,
		});
		let user = await foundUser.update({
			$inc:{
			points: req.body.tripScore,
			completedTripCount: +1  
		},
		}); 

		return res.status(200).json(trip);

	} catch (error) {
		return next(error);
	}
}

// GET - /api/users/:id/trips/not-active
// Get all non active trips (Non-expired trips)
exports.getNotActive = async function(req, res, next) {
  try {
		let trip = await db.Trip.find({
			$and: [
			{'active': false},
			{dateISO: {$gte: new Date() }}
			]
		})
		.sort({ updatedAt: "desc" })
		.populate("user", {
			name: true,        
		});
    return res.status(200).json(trip);
  } catch (err) {
    return next(err);
  }
}; 

// GET - /api/users/:id/trips/active
exports.getActive = async function(req, res, next) {
  try {
    let trip = await db.Trip.find({
			$and: [
				{'completed': false}, {'active': true},
			{$or: [
				{ 'owner.id': req.params.id},
				{'user_confirm.id': req.params.id}
			]},
			// Still display the trip if 15 minutes have gone by start time
			{dateISO: {$gte: new Date() - 1000 * 15 * 60 }}
		]
		})
		.sort({ dateISO: "desc" })
		.populate("user", {
			name: true,        
		});
    return res.status(200).json(trip);
  } catch (err) {
    return next(err);
  }
}; 

// GET - /api/users/:id/trips/trips-complete
exports.finishedTrips = async function(req, res, next) {
  try {
    let trip = await db.Trip.find({
			'$and': [
				{'completed': true},
			{'$or': [
				{ 'owner.id': req.params.id},
				{'user_confirm.id': req.params.id}
			]}
		]
		})
		.sort({ dateISO: "desc" })
		.populate("user", {
			name: true,        
		});
    return res.status(200).json(trip);
  } catch (err) {
    return next(err);
  }
}; 





