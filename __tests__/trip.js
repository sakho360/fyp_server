const frisby = require('frisby');
const HOST = 'http://localhost:1337'
const Joi = frisby.Joi; // Frisby exports Joi for convenience on type assersions
const id = '5bf2a01752540e0015b43268' //osc123@gmail.com id
const trip_id = '5c7504eaf7a8b9001655d589' // template trip id



it('POST SignUp should return status of 200', function () {
  return frisby
    .post(`${HOST}/api/auth/signup`, {
			name: 'tester1',
      email: 'test1',
      password: 'test'
    })
    .expect('status', 200);
});


// Do setup first
frisby.globalSetup({
  request: {
    headers: {
      'Authorization': 'Bearer eyJhbGciOiJIUzI1NiIsInR5cCI6IkpXVCJ9.eyJpZCI6IjViZjJhMDE3NTI1NDBlMDAxNWI0MzI2OCIsImVtYWlsIjoib3NjQGdtYWlsLmNvbSIsImlhdCI6MTU0MjgxMzg0MH0.cbL3FjcXNGsHV-B89L2D0EKDCzSsyUrNRVVH8uWfuuQ',
      'Content-Type': 'application/json',
    }
  }
});


it('POST Login should return status of 200', function () {
  return frisby
    .post(`${HOST}/api/auth/signin`, {
      email: 'osc123@gmail.com',
      password: 'osc123'
    })
    .expect('status', 200);
});

it('GET list of users - sensitive data not returned', function () {
  return frisby.get(`${HOST}/api/users`)
		.expect('status', 200)
		.expect('jsonTypesStrict', 'user.*', { // Assert *each* object in 'items' array
		'_id': Joi.string().required(),
		'__v': Joi.number().required(),
		'name': Joi.string().required(),
		'completedTripCount': Joi.number().required(),
		'points': Joi.number().required(),
	});
});

it('POST a Trip', function () {
  return frisby
    .post(`${HOST}/api/auth/signin`, {
      email: 'osc123@gmail.com',
      password: 'osc123'
    })
    .expect('status', 200);
});

it('GET list of trips', function () {
  return frisby.get(`${HOST}/api/trips`)
    .expect('status', 200);
});

it('GET a specific trip /:trip_id', function () {
  return frisby.get(`${HOST}/api/users/${id}/trips/${trip_id}`)
    .expect('status', 200);
});


it('PUT - update specific trip /:trip_id', function () {
  return frisby.put(`${HOST}/api/users/${id}/trips/${trip_id}`)
    .expect('status', 200);
});

/* it('DELETE a specific trip /:trip_id', function () {
  return frisby.get(`${HOST}/api/users/${id}/trips/${trip_id}`)
    .expect('status', 200);
}); */


it('GET all non-active trips /:trip_id', function () {
  return frisby.get(`${HOST}/api/users/${id}/trips/not-active`)
    .expect('status', 200);
});

it('GET all complete trips /:trip_id', function () {
  return frisby.get(`${HOST}/api/users/${id}/trips/tripComplete`)
    .expect('status', 200);
});

it('GET all non-active trips /:trip_id', function () {
  return frisby.get(`${HOST}/api/users/${id}/trips/active`)
    .expect('status', 200);
});


it('POST Trip complete /:trip_id', function () {
  return frisby.put(`${HOST}/api/users/${id}/trips/${trip_id}/trip-complete-user`)
    .expect('status', 200);
});














