const express = require("express");
const router = express.Router({ mergeParams: true });


const {
  createTrip,
  getTrip,
	deleteTrip,
	updateTrip,
	getNotActive,
	getActive,
	tripComplete,
	finishedTrips,
	prelimTripComplete
} = require("../handlers/trips");

// prefix - /api/users/:id/trips
router.route("/").post(createTrip);

// prefix - /api/users/:id/trips/not-active
router.route("/not-active").get(getNotActive);

// prefix - /api/users/:id/trips/trips-complete
router.route("/trips-complete").get(finishedTrips);

// prefix - /api/users/:id/trips/active
router.route("/active").get(getActive);

// prefix  - /api/users/:id/trips/:trip_id/tripComplete
router.route('/:trip_id/tripComplete').put(tripComplete);

// prefix  - /api/users/:id/trips/tripComplete
router.route('/:trip_id/trip-complete-user').put(prelimTripComplete);



// prefix - /api/users/:id/trips/:trip_id
router
  .route("/:trip_id")
	.get(getTrip)
	.put(updateTrip)
	.delete(deleteTrip)
	
	

module.exports = router;
